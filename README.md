# README for SAM FIGUEROA

This readme is about me:
- Position
- About me
- My preferences / How to work with me
- Strenghts & Weaknesses
- Setup
- FYIs

I'll continue to evolve this page as needed.

## Position

I work in the Securtiy section as part of the [Govern::Compliance](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/) sub-department as a Fullstack Engineer.

## About me

- I don't feel I have an actual place I grew up since I'm an Army brat. We got transfered quite a bit between Germany, Puerto Rico and Georgia (USA). I went to US-Army dependant schools until 7th grade and then switched to German schools. 

- I eventually ended up staying in Germany, where I finished school and met my wife.

- We have 4 kids and they fill up most of my time after work. I grew up in a big family and love having a big family of my own.

## My Preferences / How to work with me

- I value feedback in a direct and timely manor. The sooner I get input on my actions the better I can learn from them and improve. This especially requires (brutal) honesty. I do not enjoy sugar-coated feeedback that might obscure what the person is actually feeling. I like to think that I've got a fairly thick skin and short toes. If you have something on your mind don't shy to let it rip my way.

- I'm somewhat of a mixed bag between an introvert and somebody who likes social connection. I enjoy meeting new people and extending my network, and I just need to be aware that I'll need time to recharge afterwards.

- I'm not affraid to change my mind on something I've previously held hard opinons on when new data presents itself. Be ready for me to do a complete 180º if circumstances change. Also feel free to present me with data that aims to change my mind. I'm generally aware that oftem I'm not right and willing to give things a second look.

- When I'm on a Zoom call and I turn off my video, it's usually to do stretches or squats while listening, to keep from distracting people seeing me do exercises.

## Strengths & Weaknesses

- When working on a complex problem I tend to hunker down and try to figure everything out on my own, forgetting to stop and ask for help at times. If you notice me perhaps being stuck on something or me not pushing any updates on where I'm stuck feel free to ask me if I might need some help.

- I tend to get distracted easily. A reason I tend to turn off notifications where I can and try to only check e-mails and slack sporadically throughout the day. 

- I've been told I'm a good listener and can mediate well between parties. 

- Past managers and personality tests have hinted that I'm an inclusive nature and can knit teams tighter together. 

- If there's a process that isn't running smoothly I often get caught up on it and try to find improvments or at lest call it out. 

- Just as I value transparency and candor towards me I can sometimes come across as direct/harsh with my words (so my wife tells me especially). Please feel free to ask me to tone it down or rephrase things differently in the future. 

- Rabbit holes: might as well call me Alice since I tend to fall down quite a few of them. It's an area of focus for me to try and stay on task more. 

- Spelling: auto-correct is my best friend

## Setup

### Software

- Editor: neovim
- OS: macOS
- Terminal Emulator: [kitty](https://sw.kovidgoyal.net/kitty/)
- Other apps that I find quite handy:
  - Shush - cough drop or PTT for microphones
  - Alfred - quick access to things via the keyboard
  - BetterTouchTool - for keyboard shortcuts and drag zones for window managment
  - Divvy - window managment
  - [Quitter](https://marco.org/2016/05/02/quitter) - to hide or quit apps so I don't use them too much

### Hardware

- Computer: Apple MacBook Pro 16"(2021) M1 Max, 32 GB RAM
- Monitor: HP Envy 27s 4K 
- Keyboards (US querty layout): 
  - Keychron K1 TKL
  - Apple Magic Keyboard with TouchID
- Mouse: Logitech MX Master 3
- Microphone: M-Audio UBER MIC
- WebCam: Canon EOS-R connected via Elgato CAM LINK
- Lights: 
  - Elgato Key Light
  - Logitech Litra Beam

## FYIs

- Pronouns: he/him/his
- Yeah, I'm one of those vegans. Mostly I'm not pushy about it tho ;)

